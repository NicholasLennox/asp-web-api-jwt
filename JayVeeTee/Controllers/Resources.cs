﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace JayVeeTee.Controllers
{
    [Route("api/resources")]
    [ApiController]
    [Authorize]
    public class Resources : ControllerBase
    {
        [HttpGet("public")] // Anyone, dont need to be authenticated
        [AllowAnonymous]
        public IActionResult GetPublicResources()
        {
            return Ok(new { Message = "Public resources"});
        }
        [HttpGet("protected")] // Need to be authenticated (401)
        public IActionResult GetProtectedResources()
        {
            return Ok(new { Message = "Protected resources" });
        }
        [HttpGet("admin")] // Need to be authenticated and role= admin (401, 403)
        [Authorize(Roles = "Admin")]
        public IActionResult GetAdminResources()
        {
            return Ok(new { Message = "Admin resources" });
        }

        [HttpGet("user")] // Need to be authenticated and role= admin (401, 403)
        public IActionResult GetUser()
        {
            var name = User.FindFirstValue(ClaimTypes.Name);
            return Ok(new { Message = name });
        }
    }
}
